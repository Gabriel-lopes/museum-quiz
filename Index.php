<?php
require_once "Afficher.php";
require_once "Connex.php";

session_start();

include('Base de donées/ConnexionBDD.php');

?>

<!DOCTYPE html>
	<html>
	
	<head> 
		<meta charset = "utf-8 ">
		<meta http-equiv="X-UA-Compatible" content="IE-edge">
		<link rel="stylesheet" href="CSS/style.css" />
		<link rel="icon" href="pikachu.png" type="image/png"/>
		<title>Accueil</title>
	</head>
	
	<body>
		<header>
			<table style="width: 100%">
			<tr>
				<?php if(!isset($_SESSION['connexion'])){ ?>
				<th><a href="Login_signup.php"><button  class="btn menu" >Se connecter / S'inscire</button> </a></th> <?php	}else {?>
				<?php if($_SESSION['role'] == 1) { ?> 
				<th><a href="Administration.php?id=<?php echo $_SESSION['connexion']; ?>"><button  class="btn menu" >Administrer</button></a></th> <?php	} ?>
				<th><a href="UserProfil.php?id=<?php echo $_SESSION['connexion']; ?>"><button  class="btn menu" > Votre Profil</button></a></th>
				<th><a href="logout.php"><button  class="btn menu" > Déconnexion</button></a></th>
				<?php } ?>
			</tr>	
		</table></header>
		<br><br>
		<div align="center" class='accueil'>
		<h1>Bienvenue</h1>
		<p>Vous pensez avoir tout retenu de votre visite au museum ? <br>
		Testez vos connaissances en choisissant l'un des jeux ci-dessous !</p>
		<br><br>
<?php
	$connex = connexion_bd();
	$game = lire_game($connex);
	page_game($game);

	mysqli_free_result($game);
	mysqli_close($connex);
?>		
		<br><br><br>
		<?php if(isset($_SESSION['connexion'])){ ?>
		<a href="CreaGame.php"><button  class="btn menu" >Créé votre jeu !</button></a>
		<br>
		<?php } ?>
		</div>
	
	</body>
	
	</html>