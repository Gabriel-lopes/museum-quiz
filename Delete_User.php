<?php
	session_start();
	include('Base de donées/ConnexionBDD.php');
	$id = $_SESSION['id'];
	$bdd->prepare("DELETE FROM users WHERE id = ?")->execute( array($id) );
	
	$redirect_url = "Logout.php";
	header("Location: {$redirect_url}");
	exit;
 ?>