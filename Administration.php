<?php
	session_start();
	include('Base de donées/ConnexionBDD.php');
	
	//Faire en sorte que si on est pas connecté on ne peut pas avoir accès à cette page 
	if($_SESSION['role'] != 1) {
			header('Location:Index.php'); //si on est pas administrateur on est rediriger vers l'accueil
	}
	
	//Fonction qui va permettre au bouton 'supprimer' de suipprimer n'importe quel utilisateur via un simple clique
	if(isset($_GET['supprime']) AND !empty($_GET['supprime'])) {
		$supprime = (int) $_GET['supprime'];
		
		$req = $bdd->prepare('DELETE FROM users WHERE id = ?');
		$req->execute(array($supprime));
	}
	//la variable membres est assignée à tous les utilisateurs de la table users et va les afficher du plus récent inscris au plus ancien en prennant en compte leur id et adresse mail et va en afficher 10 aux maximum
	$membres = $bdd->query('SELECT * FROM users ORDER BY id DESC LIMIT 0,10');
	
	
	//Fonction qui va permettre au bouton 'supprimer' de suipprimer n'importe quel carte via un simple clique
	if(isset($_GET['delete']) AND !empty($_GET['delete'])) {
		$delete = (int) $_GET['delete'];
		
		$req = $bdd->prepare('DELETE FROM card WHERE id = ?');
		$req->execute(array($delete));
		
		/////////////////////////////Défénir $c comme étant l'id du jeu
		$jeux = $_SESSION['id_game'];
		$req = $bdd->prepare('DELETE FROM card WHERE id = ?');
		$req->execute(array($delete));
		$incre = $bdd->query("SELECT * FROM game WHERE id_game = '$jeux'");
		$game = $incre->fetch();
		$_SESSION['id_game'] = $game['id_game'];	
		$inc = $bdd->prepare("UPDATE game SET nb_card = (nb_card - 1) WHERE id_game = '$jeux'"); //Incrémentation du nombre de carte dans la table jeu (ne fonctionne pas pour le moment
		$inc->execute(array($jeux, $_SESSION['id_game']));
		
		/////////////////////////////
	}
	//la variable cartes est assignée à tous les cartes de la table card et va les afficher de la plus récente créée à la plus ancienne en prennant en compte leur id, question, réponse, jeu et créateur, et va en afficher 20 aux maximum
	$cartes = $bdd->query('SELECT * FROM card ORDER by id DESC LIMIT 0,20');

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	 <link rel="stylesheet" href="CSS/style.css" />
	 <link rel="icon" href="pikachu.png" type="image/png"/>
	 <title>Administration</title>
</head>

<body>
<a href="Index.php"><button class="btn menu">Retour</button></a>
<br><br>
<div align="center">
<h1>Page d'administration</h1>
</div>
<p> Bienvenu administrateur !</p>
<h4>Voici la liste des membres et des cartes de ce site, en cas d'infraction à notre charte de conduite à adopter, vous êtes dans le droit de supprimer le compte de l'utilisateur ayant commis l'infraction ainsi que les cartes faisaint elles aussi défaut au règlement :</h4>

<ul>
	<?php while($m = $membres->fetch()) { ?> <!-- while qui va nous permettre d'afficher un à un les élément demander tant que ceux-ci sont existant avec une limite de 10 éléments -->
	<li>
	<?= $m['id'] ?> : <?= $m['email'] ?> <!-- Affichage des élément id et email des utilisateurs -->
	- <a href="administration.php?supprime=<?= $m['id'] ?>"><button class="btn suppr">Supprimer</button></a> <!-- Création du bouton de suppression -->
	</li>
	<?php } ?>
</ul>
<br>
<hr>
<br>
<ul>
	<?php while($c = $cartes->fetch()) { ?> <!-- while qui va nous permettre d'afficher un à un les élément demander tant que ceux-ci sont existant avec une limite de 20 éléments -->
	<li>
	<?= $c['id'] ?> - <?= $c['question'] ?> - <?= $c['answer'] ?> - <?= $c['id_game'] ?> <br>ID du créateur : <?= $c['id_createur'] ?> <!-- Affichage des élément id, question, réponse, id du jeu et id du créateur des cartes -->
	- <a href="administration.php?delete=<?= $c['id'] ?>"><button class="btn suppr">Supprimer</button></a><br><br> <!-- Création d'un nouveau bouton de suppression -->
	</li>
	<?php } ?>
</ul>

</body>

</html>
