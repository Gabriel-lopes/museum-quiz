<?php
	session_start();
	include('Base de donées/ConnexionBDD.php');
	
	if(isset($_GET['id']) AND $_GET['id'] > 0) {
		$getid = intval($_GET['id']);
		$requser = $bdd->prepare('SELECT * FROM users WHERE id = ?');
		$requser->execute(array($getid));
		$UserData = $requser->fetch();
		
?>
	<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="CSS/Profil_CSS.css" />
		<link rel="icon" href="pikachu.png" type="image/png"/>
		<title>Profil</title>
	</head> 
	
	<body>
	<!-- Ici on affiche grâce aux variables de session les informations des utilisateurs : -->
		<div class="profil" align="center">
			<h2> Profil de <?php echo $_SESSION['prenom']; ?> </h2>
			<br>
			<?php 
			if($_SESSION['role'] == 1) {   //Si vous êtes administrateur on récupère la variable de session 'role' qui sera nécéssairement égale à 1
			echo '<p> Vous êtes Administrateur<p>',"<br>"; //affiche votre rôle d'administrateur si vous l'êtes
		} // sinon n'affiche rien

			?>
			
			<tr><div class="info">Il se peut que la modification de votre profil ne soit pas immédiatement perceptible ici, si cela est le cas nous vous invitons à bien vouloir vous reconnecter.</div></tr>
			<br><br>
<table>
			<tr> <td>E-mail :  <?php echo $_SESSION['email']; ?></td></tr>
			<tr><td> Nom :  <?php echo $_SESSION['nom']; ?></td></tr>
			<tr><td> Prénom :  <?php echo $_SESSION['prenom']; ?> </td></tr>
			<tr><td> Vous êtes inscrit depuis le <?php echo $_SESSION['date']; ?> </td></tr>
</table>
			<br><br>
			<table>
			<tr>
			<?php 
				if(isset($_SESSION['id']) AND $UserData['id'] == $_SESSION['id']) { ?>
					<a href="EditProfil.php"><button class="btn menu" >Editer le profil</button></a></tr>
				<?php }
			?><tr>
			<a href="Index.php"><button  class="btn menu" >Vers l'accueil</button></a></tr>
			<div class="Logout"> <a href="logout.php"><button class="btn menu" > Déconnexion</button></a></div>
</table>
	<br><br><br>
			<a href="Delete_User.php?id=<?php echo $_SESSION['id'];?>"><button  class="btn suppr" >Supprimer votre compte</button></a>
			
		</div>
	</body>
	
	</html>
	<?php 
	}
	?>