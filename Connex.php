<?php
 require_once "Afficher.php";

 function connexion_bd() {
	$serv ="127.0.0.1";
	$username = "root";
	$pwd = "";
	$bd ="projetl";
	$connex = mysqli_connect($serv, $username,$pwd, $bd);
	if (! $connex) {
		page_erreur(ERR_CONNEX,mysqli_connect_error($connex)); 
		exit;
 	}
 	return $connex;
}

function lire_game($connex) {
	$req = "SELECT * FROM game";
	$resultat = mysqli_query($connex, $req);
	if (!$resultat) {
		page_erreur(ERR_REQUETE,mysqli_error($connex));
		mysqli_close($connex);
		exit;
 	}
 	return $resultat;
}

function lire_card($connex) {
	$req = "SELECT question FROM card WHERE id_game = GET[id_game] LIMIT 1"; // quelle methode GET OU POST????
	$resultat = mysqli_query($connex, $req);
	if (!$resultat) {
		page_erreur(ERR_REQUETE,mysqli_error($connex));
		mysqli_close($connex);
		exit;
 	}
 	return $resultat;
}
?>