<?php
	session_start();
	include('Base de donées/ConnexionBDD.php');
	include('Securisation.php');
	
	if(isset($_SESSION['id'])) { //Si la session existe bien (l'utilisateur est connecté) :
		$requser = $bdd->prepare('SELECT * FROM users WHERE id = ?'); //la variable requser va contenir toutes les infos de l'utilisateur connecté
		$requser->execute(array($_SESSION['id']));
		$user = $requser->fetch();
		
		//Formulaire de modification pour le NOM :
		if(isset($_POST['newnom']) AND !empty($_POST['newnom']) AND $_POST['newnom'] != $user['nom']) { //Si les champs sont bien remplies
			$newnom = htmlspecialchars($_POST[newnom]);
			$insertNom = $bdd->prepare("UPDATE users SET nom = ? WHERE id = ?"); //modification dans la base de données
			$insertNom->execute(array($newnom, $_SESSION['id']));
			header('location:UserProfil.php?id='.$_SESSION['id']); //redirection une fois soumis via le bouton submit
		}
		
		//Formulaire de modification pour le PRENOM :
		if(isset($_POST['newprenom']) AND !empty($_POST['newprenom']) AND $_POST['newprenom'] != $user['prenom']) { //Si les champs sont bien remplies
			$newprenom = htmlspecialchars($_POST[newprenom]);
			$insertPrenom = $bdd->prepare("UPDATE users SET prenom = ? WHERE id = ?"); //modification dans la base de données
			$insertPrenom->execute(array($newprenom, $_SESSION['id']));
			header('location:UserProfil.php?id='.$_SESSION['id']); //redirection une fois soumis via le bouton submit
		}
		
		//Formulaire de modification pour le MAIL :   LES DOUBLONS D'email sont toujours possible
		if(isset($_POST['newemail']) AND !empty($_POST['newemail']) AND $_POST['newemail'] != $user['email']) { //Si les champs sont bien remplies
			$newemail = htmlspecialchars($_POST[newemail]); 
			$insertEmail = $bdd->prepare("UPDATE users SET email = ? WHERE id = ?"); //modification dans la base de données
			$insertEmail->execute(array($newemail, $_SESSION['id']));
			header('location:UserProfil.php?id='.$_SESSION['id']); //redirection une fois soumis via le bouton submit
		}
		
		//Formulaire de modification pour le MOT DE PASSE :
		if(isset($_POST['newmdp1']) AND !empty($_POST['newmdp1']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2'])) { //Si les champs sont bien remplies et les deux mots de passe différents
			$mdp1 = SecureMDP($_POST['newmdp1']);
			$mdp2 = SecureMDP($_POST['newmdp2']);
			
			if($mdp1 == $mdp2) {
				$insertMdp = $bdd->prepare("UPDATE users SET mdp = ? WHERE id = ?"); //modification dans la base de données
				$insertMdp->execute(array($mdp1, $_SESSION['id']));
				header('location:UserProfil.php?id='.$_SESSION['id']); //redirection une fois soumis via le bouton submit
			} else {
				$return = "Le mot de passe n'a pas été confirmé correctement"; //Message d'erreur
			}
			
		}
?>
	<html>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="CSS/style.css" />
		<link rel="icon" href="pikachu.png" type="image/png"/>
		<title>Editer mon profil</title>
	<head> 
	
	<body>
		<div align="center" class="accueil">
			<h2>Edition de votre Profil</h2>
			<br>
			<!-- Formulaire de modifications de ses propres données : -->
			
			<form method="POST" action="#">
				<label>Nom : </label>
				<input type="text" name="newnom" placeholder="Nouveau Nom" value="<?php echo $user['nom'] ?>" > <br>
				<label>Prenom : </label>
				<input type="text" name="newprenom" placeholder="Nouveau Prenom" value="<?php echo $user['prenom'] ?>" > <br>
				<label>E-mail : </label>
				<input type="text" name="newemail" placeholder="Nouvelle E-mail"value="<?php echo $user['email'] ?>" > <br>
				<label> Mot de passe : </label>
				<input type="password" name="newmdp1" placeholder="Nouveau Mot de passe"> <br>
				<label> Confirmation du mot de passe : </label>
				<input type="password" name="newmdp2" placeholder="Confirmation nouveau mot de passe"> <br> <br>
				
				<input class="btn menu"type="submit" name="modification" value="Changer vos informations">
			</form>
			<?php 
				if(isset($return)) { //Fonction affichant le message d'erreur si nécessaire
				echo $return;
				}
			?>
		</div>
	</body>
	
	</html>
	<?php 
	} else {
		header('location:Index.php');
	}
	?>
	