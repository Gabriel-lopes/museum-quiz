<?php
	session_start();
	include('Securisation.php');
	include('Base de donées/ConnexionBDD.php'); 
	
	//Formulaire d'inscription (variables et conditions) :
	if(isset($_POST['inscription'])) {
		$nom = Secure($_POST['nom']);
		$prenom = Secure($_POST['prenom']);
		$mdp = Secure($_POST['mdp']);
		$mdp2 = Secure($_POST['mdp2']);
		$email = Secure($_POST['email']);
		$date = date('d/m/Y à H:i:s');
		
			if(!empty($nom) AND !empty($prenom) AND !empty($email) AND !empty($mdp) AND !empty($mdp2)) {
				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
					if($mdp == $mdp2) {
						if(strlen($nom) <= 20) {
							$TestEmail = $bdd->query('SELECT id FROM users WHERE email = "'.$email.'"');
							if($TestEmail->rowCount() < 1) {
								$mdp = SecureMDP($mdp); // on change le mdp de l'utilisateur pour lui ajouter la fonction qui va le sécurisé
								$bdd->query('INSERT INTO users (nom, prenom, email, mdp, date) VALUES ("'.$nom.'", "'.$prenom.'", "'.$email.'", "'.$mdp.'", "'.$date.'")');
								$return = "Utilisateur créé, vous pouvez désormais vous connecter !";
							} else $return = "Adresse email est déjà utilisée.";
						} else $return = "Votre nom à plus de 20 caractères.";
					} else $return = "La confirmation du mot de passe n'est pas correcte.";
				} else $return = "e-mail non valide.";
			} else $return = "Champs manquants.";
	}
	
	
	
	//Formulaire de connexion (variables et conditions) :
	if(isset($_POST['connexion'])) {
		$email = Secure($_POST['email']);
		$mdp = Secure($_POST['mdp']);
		
			if(!empty($email) AND !empty($mdp)) {
				$mdp = SecureMDP($mdp); // On crypte le mdp de connexion pour qu'il soit identique à celui de l'inscription (vus qu'il est crypté dans la bdd) 
				$VerifUser = $bdd->query('SELECT * FROM users WHERE email = "'.$email.'" AND mdp = "'.$mdp.'"');
				$UserData = $VerifUser->fetch();
				if($VerifUser->rowCount() == 1) {
					$_SESSION['connexion'] = $UserData['id']; //si toutes les conditions sont remplies, la session connexion est crée
					$_SESSION['id'] = $UserData['id'];
					$_SESSION['prenom'] = $UserData['prenom'];
					$_SESSION['nom'] = $UserData['nom'];
					$_SESSION['email'] = $UserData['email'];
					$_SESSION['role'] = $UserData['role'];
					$_SESSION['date'] = $UserData['date'];
					//$return = "vous êtes connecté";
					header('location:Index.php?id='.$_SESSION['connexion']);
					//header('location:UserProfil.php?id='.$_SESSION['connexion']);
				} else $return = "Les identifiants sont invalides.";
			} else $return = "Champs manquants.";
	}
	
?>

<!DOCTYPE html>
<html lang="fr">	
	<head>
		<meta charset = "UTF-8">
		<meta http-equiv="x-UA-Compatible" content="IE-edge">
		<title>Formulaire</title>
		<link rel="stylesheet" href="CSS/style.css" />
		<link rel="icon" href="pikachu.png" type="image/png"/>
	</head>
	<body>
		<div class="connex">
		<h1>Inscription</h1>
		<br>
		<!-- Formulaire d'inscription en html -->
		<?php if(isset($_POST['inscription']) AND isset($return)) echo $return; ?>
		<form action="#" method="POST">	
			<input class="btn carte" type="text" name ="nom" placeholder="Nom">
			<input class="btn carte" type="text" name ="prenom" placeholder="Prénom">
			<input class="btn carte" type="email" name="email" placeholder="e-mail">
			<input class="btn carte" type="password" name ="mdp" placeholder="Mot de passe">
			<input class="btn carte" type="password" name ="mdp2" placeholder="Confirmation du mot de passe">
			<input class="btn menu" type="submit" name="inscription" value="Je m'inscris">
		</form>
		<hr>
		<br>
	
		<h1>Connexion</h1>
		<br>
		<!-- Formulaire de connexion en html -->
		<?php if(isset($_POST['connexion']) AND isset($return)) echo $return; ?>
		<form action="#" method="POST">
			<input class="btn carte" type="email" name="email" placeholder="Votre e-mail">
			<input class="btn carte" type="password" name ="mdp" placeholder="Votre mot de passe">
			<input class="btn menu" type="submit" name="connexion" value="Se connecter">
		</form>
		<hr>
		<br>
		</div>
	</body>
</html>	