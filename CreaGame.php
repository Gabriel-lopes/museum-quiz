<?php
	Session_start();
	include('Base de donées/ConnexionBDD.php');
	require_once "Connex.php";
	
	$id_crea = $_SESSION['connexion'];
	//echo $id_crea;
	
	if(!isset($_SESSION['connexion'])){
		header('location:Index.php');
	}
	
	if(isset($_POST['creation'])) {
		$question = $_POST['question'];
		$reponse = $_POST['reponse'];
		$jeux = $_POST['jeux'];
		
		
			if(!empty($question) AND !empty($reponse) AND !empty($jeux)) {
				$verifyQ = $bdd->query('SELECT question FROM card WHERE question = "'.$question.'"');
				if($verifyQ->rowCount() < 1) {
					$incre = $bdd->query("SELECT * FROM game WHERE id_game = '$jeux'");
					$game = $incre->fetch();
					$_SESSION['id_game'] = $game['id_game'];	
					$bdd->query('INSERT INTO card (id_game, question, answer, id_createur) VALUES ("'.$jeux.'", "'.$question.'", "'.$reponse.'", "'.$id_crea.'")');
					$inc = $bdd->prepare("UPDATE game SET nb_card = (nb_card + 1) WHERE id_game = '$jeux'"); //Incrémentation du nombre de carte dans la table jeu (ne fonctionne pas pour le moment
					$inc->execute(array($jeux, $_SESSION['id_game']));
					$return = "Carte créée !";
					//header('location:Accueil.php');
				}else $return = "Question déjà existente";
			}else $return = "Un des champs n'est pas remplie";
	}
	
?>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="CSS/style.css" />
		<link rel="icon" href="pikachu.png" type="image/png"/>
		<title>Créez votre carte!</title>
	</head>
	
	
	<body>
		<a href="Index.php"><button class="btn menu">Retour</button></a>
		<br>
	    <div align="center">
			<h1>Créez votre carte !</h1><br>
			</div>
			<h4>Attention votre jeu doit respecter notre charte de conduite.<br>
			Si une ou plusieurs des caractéristiques suivantes se trouvent dans votre jeu, tout administrateurs se réservera le droit de supprimer votre jeu et selon la gravité de l'infraction votre compte également : <br><br>
			-> Propos haineux <br>
			-> Discrimination <br>
			-> Provocation <br>
			-> Acharnement verbal <br>
			-> Propos sexuel <br>
			-> Homophobie <br>
			-> Racisme <br>
			-> Sexisme <br>
			-> Informations érronées</h4>
			
		
		<br><br><br>
		<div align="center">
		<?php if(isset($_POST['creation']) AND isset($return)) echo $return; ?>
			<form action="#" method="POST">
		
				<label>Question : </label>
					<input type="text" name="question" size="60" style="height:30px;"> <br><br>
					
				<label>Réponse : </label>
					<input type="text" name="reponse" size="100" style="height:80px;"> <br><br><br>
					
				<label>Jeux : </label>
					<select id="jeux" name="jeux">
					  <option value="1">Météorites</option>
					  <option value="2">Minéralogie</option>
					</select>
					<br><br><br>
				<input class="btn menu" type="submit" name="creation" value="Ajouter cette carte">
			</form>
		</div>
	</body>
	
</html>