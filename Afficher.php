<?php
define('ERR_CONNEXION', 0);
define ('ERR_REQUETE', 1);

//---------------------------------------------
//AFFICHER JEU DANS TABLEAU A LA PAGE D'ACCUEIL//
//---------------------------------------------

//affiche une ligne de la table game
//protection htmlspecialchars
function afficher_ligne(&$ligne) {
	if(htmlspecialchars($ligne['nb_card'])>0){//Si il n'y a pas de carte on affiche pas
	$id_game=htmlspecialchars($ligne["id_game"]);
	$cpt=(int) 0;
	echo "<tr>",
	"<td align='center'><a href='Party.php?id_g=$id_game&cpt=$cpt'><button  class='btn carte' >".htmlspecialchars($ligne["nom"])."</button></a></td>",
	"<td align='center'>".htmlspecialchars($ligne["description"])."</td>",
	"<td align='center'>".htmlspecialchars($ligne["nb_card"])."</td>",
	"</tr>";
	}
}


//affiche les colonnes de la table game
function afficher_table ($game) { ?>
	<table align="center">
		<tr> <th>Nom du jeu</th>
		<th>Description</th>
		<th>Nombre de Cartes</th></tr>

<?php
	while($ligne = mysqli_fetch_assoc($game))
 		afficher_ligne($ligne);
 	echo "</table>" ;
}

//page qui affiche les jeux
 function page_game ($game) {
 	echo "<h2 align='center'> Les jeux </h2>";
 	afficher_table($game);
 }
 //Affiche page erreur
function page_erreur($err_code, $error) {
 echo "Erreur";
 switch ($err_code) {
	 case ERR_CONNEXION:
	 echo "<h2>Desolé, connexion impossible</h2>";
	 break;
	 case ERR_REQUETE:
	 echo "<h2>Erreur dans l'execution de la requete</h2>";
	 break;
 }
  echo "<p>".$error."</p>";
}
?>
