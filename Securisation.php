<?php
	//page contenant les fonctions permettant la sécurité des données :
	
	function Secure($string) {
		if(ctype_digit($string)) { // si le type string est un int
			$string = intval($string);
		}else{
			$string = strip_tags($string); //pour éviter les injections html
			$string = addcslashes($string, '%_'); //pour éviter certain caractères
		}
		return $string;
	}


	//fonction qui crypte le mot de passe :
	function SecureMDP($s) {
		$s = sha1(md5('àzérvybùio?p'.$s));
		return $s;
	}
	
?>	